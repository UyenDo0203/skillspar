$(window).on("load", function () {
    selectOptionCustom()
    MenuToggleMB()
    sliderHome()
    GotoBlock2Home()
    activeMenu()
    videoAction()

    new WOW().init();

    $('.loading').removeClass('active')
});

$(window).on("scroll", function () {
    let headerHeight = $('.header').height()
    let windowHeight = $(window).scrollTop()
    if (windowHeight > (headerHeight + 10)) {
        $('.header').addClass('fixed')
    } else {
        $('.header').removeClass('fixed')
    }
});

$(window).on("resize", function () {
    if (window.innerWidth > 1024) {
        $('.header-menu__mb').removeClass('active')
        $('.header-menu__nav').removeClass('active')
    }
});

let videoAction = function () {
    let player;
    let idVideo = $('.video__play')[0] && $('.video__play')[0].attributes.video && $('.video__play')[0].attributes.video.value;

    onYouTubeIframeAPIReady()
    function onYouTubeIframeAPIReady() {
        player = new YT.Player('player', {
            height: '390',
            width: '640',
            videoId: idVideo,
            playerVars: {
                color: 'white',
                // controls: 0,
                rel: 0,
                enablejsapi: 1,
                modestbranding: 1, showinfo: 0, ecver: 2
            },
            events: {
                'onReady': onPause,
                'onStateChange': onPlayerStateChange
            }
        });
    }

    function onPause(event) {
        event.target.pauseVideo();
    }

    var done = false;
    function onPlayerStateChange(event) {
        if (event.data == YT.PlayerState.PLAYING && !done) {
            done = true;
        }
    }

    function playVideoActiveClass(status) {
        if (status) {
            $('.video__pause').addClass('inactive')
            $('.video__play').addClass('active')
            player.playVideo();
        } else {
            $('.video__pause').removeClass('inactive')
            $('.video__play').removeClass('active')
            player.stopVideo();
        }

    }

    // play 
    $('.video__pause .play-icon').click(function () {
        playVideoActiveClass(true)
    })

    // trigger play 
    $('#playVideoTrigger').click(function () {
        playVideoActiveClass(true)
    })
}

let activeMenu = function () {
    let namePage = window.location.pathname.split('.html')
    $('.header-menu__nav li').each(function (key, v) {
        let objectE = { v }
        let objectEValue = objectE.v.attributes[0] && objectE.v.attributes[0].value
        if (namePage[0].indexOf(objectEValue) != -1) {
            objectE.v.className = 'active'
        }
    });
}

let sliderHome = function () {
    if ($(".home-slider__wrap").length === 0) {
        return false
    }

    $(".home-slider__wrap").slick({
        arrows: false,
        dots: true,
        autoplay: true,
        autoplaySpeed: 3000,
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
    });
}

let selectOptionCustom = function () {
    $('.form-select').addClass('no-select')
    $('.form-select').change(function () {
        // console.log($(this).val());
        let value = $(this).val()
        if (value == '' || value == 0) {
            $(this).addClass('no-select')
        } else {
            $(this).removeClass('no-select')
        }
    })
}

let MenuToggleMB = function () {
    $('.header-menu__mb').click(function () {
        $(this).toggleClass('active')
        $('.header-menu__nav').toggleClass('active')
    })
}

let GotoBlock2Home = function () {
    $('#toNextBlock').click(function (e) {
        e.preventDefault()
        $('html, body').animate({
            scrollTop: $(".home__1").offset().top - 100
        }, 1000);
    });
}


